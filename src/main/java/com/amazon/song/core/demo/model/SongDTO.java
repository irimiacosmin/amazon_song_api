package com.amazon.song.core.demo.model;

import lombok.Value;

import java.time.LocalDate;

@Value
public class SongDTO {
    private String name;
    private Genre genre;
    private ArtistDTO artist;
    private LocalDate releaseDate;
}
