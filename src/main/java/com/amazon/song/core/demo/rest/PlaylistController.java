package com.amazon.song.core.demo.rest;

import com.amazon.song.core.demo.model.PlaylistDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/playlists")
@RequiredArgsConstructor
public class PlaylistController {

    private final SongService songService;

    @GET
    @Path("/{userId}/{name}")
    public Optional<PlaylistDTO> findFirstFor(@PathParam("userId") Integer userId, @PathParam("name") String name) {
        return songService.getPlayListBy(userId, name);
    }

    @GET
    @Path("/{userId}")
    public List<PlaylistDTO> findAllFor(@PathParam("userId") Integer userId) {
        return songService.getPlayListsBy(userId);
    }

    @GET
    @Path("/{userId}/{artistName}")
    public List<PlaylistDTO> findAllForArtistName(@PathParam("userId") Integer userId, @PathParam("artistName") String artistName) {
        return songService.getPlayListsByArtistName(userId, artistName);
    }

    @GET
    @Path("/{userId}/{genre}")
    public List<PlaylistDTO> findAllForGenre(@PathParam("userId") Integer userId, @PathParam("genre") String genre) {
        return songService.getPlayListsByGenre(userId, genre);
    }

    @POST
    @Path("/{userId}/")
    public PlaylistDTO createPlayList(@PathParam("userId") Integer userId, @PathParam("name") String name) {
        return songService.add(userId, name);
    }

    @PUT
    @Path("/{userId}/{playListId}/{songName}")
    public Optional<PlaylistDTO> addSong(@PathParam("userId") Integer userId, @PathParam("playListId") Integer playListId,
                                         @PathParam("songName") String songName) {
        return songService.addSongToPlayList(userId, playListId, songName);
    }
}
