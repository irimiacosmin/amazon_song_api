package com.amazon.song.core.demo.rest;

import com.amazon.song.core.demo.model.ArtistDTO;
import com.amazon.song.core.demo.model.Genre;
import com.amazon.song.core.demo.model.PlaylistDTO;
import com.amazon.song.core.demo.model.SongDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SongRepository {

    private static List<SongDTO> songDTOS = new ArrayList<>() {
        {
            add(new SongDTO("SOng name 1", Genre.POP, new ArtistDTO("Artist name 1", 30), LocalDate.of(2018, 10, 12)));
            add(new SongDTO("SOng name 2", Genre.POP, new ArtistDTO("Artist name 2", 12), LocalDate.of(1997, 10, 12)));
            add(new SongDTO("SOng name 3", Genre.POP, new ArtistDTO("Artist name 3", 32), LocalDate.of(2002, 10, 12)));
            add(new SongDTO("SOng name 4", Genre.POP, new ArtistDTO("Artist name 4", 43), LocalDate.of(2004, 10, 12)));
            add(new SongDTO("SOng name 5", Genre.POP, new ArtistDTO("Artist name 5", 65), LocalDate.of(2016, 10, 12)));
            add(new SongDTO("SOng name 6", Genre.POP, new ArtistDTO("Artist name 6", 74), LocalDate.of(2018, 10, 12)));
            add(new SongDTO("SOng name 7", Genre.POP, new ArtistDTO("Artist name 7", 12), LocalDate.of(2020, 10, 12)));
        }
    };

    private static List<PlaylistDTO> playlistDTOS = new ArrayList<>() {
            {
                add(new PlaylistDTO(1, "Playlist 1", getSongs().subList(0, 2)));
                add(new PlaylistDTO(2, "Playlist 2", getSongs().subList(2, 5)));
                add(new PlaylistDTO(1, "Playlist 3", getSongs().subList(2, 6)));
            }
        };

    public static List<SongDTO> getSongs() {
        return songDTOS;
    }

    public static List<PlaylistDTO> getPlaylists() {
        return playlistDTOS;
    }

    public static SongDTO addSong(SongDTO songDTO) {
        songDTOS.add(songDTO);
        return songDTO;
    }

    public static PlaylistDTO addPlayList(PlaylistDTO playlistDTO) {
        playlistDTOS.add(playlistDTO);
        return playlistDTO;
    }

    public static PlaylistDTO addSongToPlayList(int playListId, SongDTO songDTO) {
        Optional<PlaylistDTO> playlistDTOOptional = playlistDTOS.stream()
                .filter(playlistDTO -> playlistDTO.getId() == playListId)
                .findFirst();
        if (playlistDTOOptional.isPresent()) {
            PlaylistDTO playlistDTO = playlistDTOOptional.get();
            playlistDTO.addSong(songDTO);
            return playlistDTO;
        }
        return null;
    }
}
