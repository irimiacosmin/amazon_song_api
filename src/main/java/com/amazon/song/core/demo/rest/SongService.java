package com.amazon.song.core.demo.rest;

import com.amazon.song.core.demo.model.ArtistDTO;
import com.amazon.song.core.demo.model.Genre;
import com.amazon.song.core.demo.model.PlaylistDTO;
import com.amazon.song.core.demo.model.SongDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class SongService {

    public List<SongDTO> getSongsBy(String name) {
        return SongRepository.getSongs().stream()
                .filter(songDTO -> songDTO.getName().equals(name))
                .collect(Collectors.toList());
    }

    public Optional<PlaylistDTO> getPlayListBy(int userId, String name) {
        return SongRepository.getPlaylists().stream()
                .filter(playlistDTO -> playlistDTO.getUserId() == userId)
                .filter(playlistDTO -> playlistDTO.getName().equals(name))
                .findFirst();
    }

    public Optional<PlaylistDTO> getPlayListBy(int userId, int playListId) {
        return SongRepository.getPlaylists().stream()
                .filter(playlistDTO -> playlistDTO.getUserId() == userId)
                .filter(playlistDTO -> playlistDTO.getId() == playListId)
                .findFirst();
    }

    public List<PlaylistDTO> getPlayListsBy(Integer userId) {
        return SongRepository.getPlaylists().stream()
                .filter(playlistDTO -> playlistDTO.getUserId() == userId)
                .collect(Collectors.toList());
    }

    public List<PlaylistDTO> getPlayListsByArtistName(Integer userId, String artistName) {
        return SongRepository.getPlaylists().stream()
                .filter(playlistDTO -> playlistDTO.getUserId() == userId)
                .filter(playlistDTO -> playlistDTO.getSongs().stream().anyMatch(songDTO -> songDTO.getArtist().getName().equals(artistName)))
                .collect(Collectors.toList());
    }

    public List<PlaylistDTO> getPlayListsByGenre(Integer userId, String genre) {
        return SongRepository.getPlaylists().stream()
                .filter(playlistDTO -> playlistDTO.getUserId() == userId)
                .filter(playlistDTO -> playlistDTO.getSongs().stream().anyMatch(songDTO -> songDTO.getGenre().equals(Genre.valueOf(genre.toUpperCase()))))
                .collect(Collectors.toList());
    }

    public SongDTO add(String name, String genre, String artistName, int artistAge, String date) {
        SongDTO songDTO = new SongDTO(name, Genre.valueOf(genre), new ArtistDTO(artistName, artistAge), LocalDate.parse(date));
        return add(songDTO);
    }

    public SongDTO add(SongDTO songDTO) {
        return SongRepository.addSong(songDTO);
    }

    public PlaylistDTO add(int userId, String name) {
        PlaylistDTO playlistDTO = new PlaylistDTO(userId, name, new ArrayList<>());
        return add(playlistDTO);
    }

    public PlaylistDTO add(PlaylistDTO playlistDTO) {
        return SongRepository.addPlayList(playlistDTO);
    }

    public Optional<PlaylistDTO> addSongToPlayList(Integer userId, Integer playListId, String songName) {
        List<SongDTO> songDTOS = getSongsBy(songName);
        songDTOS.forEach(song -> SongRepository.addSongToPlayList(playListId, song));
        return getPlayListBy(userId, playListId);
    }
}
