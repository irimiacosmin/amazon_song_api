package com.amazon.song.core.demo.model;

public enum Genre {
    ROCK, POP, RAP, TECHNO
}
