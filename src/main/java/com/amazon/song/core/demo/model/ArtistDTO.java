package com.amazon.song.core.demo.model;

import lombok.Value;

@Value
public class ArtistDTO {
    private String name;
    private int age;
}
