package com.amazon.song.core.demo.rest;

import com.amazon.song.core.demo.model.SongDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/songs")
@RequiredArgsConstructor
public class SongController {

    private final SongService songService;

    @GET
    @Path("/{name}")
    public List<SongDTO> findFirstFor(@PathParam("name") String name) {
        return songService.getSongsBy(name);
    }

    @POST
    @Path("/")
    public SongDTO addSong(SongDTO songDTO) {
        return songService.add(songDTO);
    }
}
