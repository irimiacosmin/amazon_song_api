package com.amazon.song.core.demo.model;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
public class PlaylistDTO {
    private static List<Integer> existingIds = new ArrayList<>();
    private Integer id;
    private int userId;
    private String name;
    private List<SongDTO> songs;

    public PlaylistDTO(int userId, String name, List<SongDTO> songs) {
        this.id = existingIds.get(existingIds.size() - 1) + 1;
        existingIds.add(this.id);
        this.userId = userId;
        this.name = name;
        this.songs = songs;
    }

    public void addSong(SongDTO songDTO) {
        this.songs.add(songDTO);
    }
}
